<?php

namespace Threes;

class Board implements BoardInterface
{
    private $validMoves = ['up', 'down', 'left', 'right'];

    private $grid = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ];

    public function getId()
    {
        return spl_object_hash($this);
    }

    public function getGrid()
    {
        return $this->grid;
    }

    public function setGrid(array $grid)
    {
        $this->grid = $grid;
    }

    public function move($direction)
    {
        $this->assertValidMove($direction);
        call_user_func([$this, $direction]);
    }

    private function up()
    {
        for ($l = 1; $l < 4; $l++) {
            for ($c = 0; $c < 4; $c++) {
                $this->merge($l, $c, $l - 1, $c);
            }
        }
    }

    private function down()
    {
        for ($l = 2; $l >= 0; $l--) {
            for ($c = 0; $c < 4; $c++) {
                $this->merge($l, $c, $l + 1, $c);
            }
        }
    }

    private function left()
    {
        for ($l = 0; $l < 4; $l++) {
            for ($c = 1; $c < 4; $c++) {
                $this->merge($l, $c, $l, $c - 1);
            }
        }
    }

    private function right()
    {
        for ($l = 0; $l < 4; $l++) {
            for ($c = 2; $c >= 0; $c--) {
                $this->merge($l, $c, $l, $c + 1);
            }
        }
    }

    private function merge($line, $column, $nextLine, $nextColumn)
    {
        $currentValue = $this->grid[$line][$column];
        $nextValue = $this->grid[$nextLine][$nextColumn];

        if ($this->areMergeable($nextValue, $currentValue)) {
            $this->grid[$nextLine][$nextColumn] = $currentValue + $nextValue;
            $this->grid[$line][$column] = 0;
        }
    }

    private function areMergeable($nextValue, $currentValue)
    {
        return
            $nextValue === 0
            ||
            in_array([$currentValue, $nextValue], [[1, 2], [2, 1]])
            || $currentValue === $nextValue && $nextValue > 2;
    }

    private function assertValidMove($move)
    {
        if (!$this->isValidMove($move)) {
            throw new \InvalidArgumentException(
                sprintf('Invalid move "%s", valid moves are [%s]', $move, implode(', ', $this->validMoves))
            );
        }
    }

    public function isValidMove($move)
    {
        return
            in_array($move, $this->validMoves, true)
            &&
            is_callable([$this, $move]);
    }

    public function getValidMoves()
    {
        return $this->validMoves;
    }

    public function canMove($direction)
    {
        $tempBoard = clone $this;
        call_user_func([$tempBoard, 'move'], $direction);

        return $tempBoard->getGrid() != $this->getGrid();
    }

    public function isNotOver()
    {
        foreach ($this->getValidMoves() as $move) {
            if ($this->canMove($move)) {
                return true;
            }
        }
        return false;
    }

    public function isOver()
    {
        return !$this->isNotOver();
    }
}
