<?php

namespace Threes;

interface BoardInterface
{
    /**
     * A unique identifier
     *
     * @return mixed
     */
    public function getId();

    /**
     * multi-dimensional  array [[row],[row]]
     * ie:
     * [
     *   [1,2,3],
     *   [1,0,0],
     *   [0,3,2],
     * ]
     *
     * @return array
     */
    public function getGrid();

    /**
     * @param array $grid
     * @return void
     */
    public function setGrid(array $grid);

    /**
     * @param $direction
     * @return void
     */
    public function move($direction);

    /**
     *
     * @return array
     */
    public function getValidMoves();

    /**
     * @return boolean
     */
    public function isOver();
}