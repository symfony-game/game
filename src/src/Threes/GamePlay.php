<?php

namespace Threes;

class GamePlay
{
    /** @var  BoardRepositoryInterface */
    private $boardRepository;
    /** @var  GameBoardFactory */
    private $gameBoardFactory;

    /**
     * Game constructor.
     * @param BoardRepositoryInterface $boardRepository
     * @param GameBoardFactory $gameBoardFactory
     */
    public function __construct(BoardRepositoryInterface $boardRepository, GameBoardFactory $gameBoardFactory)
    {
        $this->boardRepository = $boardRepository;
        $this->gameBoardFactory = $gameBoardFactory;
    }

    public function restart()
    {
        $newBoard = $this->gameBoardFactory->create();
        $this->boardRepository->save($newBoard);

        return $newBoard;
    }

    public function getActiveBoard()
    {
        return $this->getOrCreateActiveBoard();
    }

    public function play($action, BoardInterface $board = null)
    {
        if (!$board) {
            $board = $this->getOrCreateActiveBoard();
        }
        $board->move($action);
        $this->boardRepository->save($board);
    }

    private function getOrCreateActiveBoard()
    {
        $board = $this->boardRepository->findLastCreatedBoard();
        if (!$board) {
            $board = $this->gameBoardFactory->create();
            $this->boardRepository->save($board);
        }
        return $board;
    }
}
