<?php

namespace AppBundle\Controller\Game;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;

class HistoryController extends Controller
{
    /**
     * @ApiDoc(
     *  description="View history"
     * )
     * @Route()
     * @View()
     */
    public function getBoardsAction()
    {
        return $this->get('app.history')->getLastGames(10);
    }
}
