<?php

namespace AppBundle\Controller\Game;

use AppBundle\Entity\Board;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PlayController extends Controller
{
    /**
     * @ApiDoc(
     *  description="Start a new game"
     * )
     * @View()
     */
    public function newAction()
    {
        $game = $this->get('app.game');
        return $game->restart();
    }

    /**
     * @ApiDoc(
     *  description="Play"
     * )
     * @Route()
     * @ParamConverter("board", class="AppBundle:Board")
     * @RequestParam(name="board", description="board id", nullable=true)
     * @View()
     */
    public function playAction($direction = null, Board $board = null)
    {
        $game = $this->get('app.game');

        if (null === $board) {
            $board = $game->getActiveBoard();
        }

        $direction && $game->play($direction, $board);

        return $board;
    }

    /**
     * @ApiDoc(
     *  description="Get board"
     * )
     * @Route()
     * @ParamConverter("board", class="AppBundle:Board")
     * @View()
     */
    public function getBoardAction(Board $board = null)
    {
        $game = $this->get('app.game');

        if (null === $board) {
            $board = $game->getActiveBoard();
        }

        return $board;
    }
}
