<?php

namespace AppBundle\Serializer;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\Routing\Router;

class AddPlayLinks implements EventSubscriberInterface
{
    /** @var  Router */
    private $router;

    /**
     * AddPlayLinks constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onSerializerPostSerialize(ObjectEvent $event)
    {
        $validMoves = [];
        $board = $event->getObject();
        foreach ($board->getValidMoves() as $move) {
            $validMoves[$move] = $this->router->generate('play', ['direction' => $move, 'board' => $board->getId()]);
        }
        $event->getVisitor()->setData('validMoves', $validMoves);
        $event->getVisitor()->setData('link', $this->router->generate('get_board', ['board' => $board->getId()]));
    }

    public static function getSubscribedEvents()
    {
        return [[
            'event' => 'serializer.post_serialize',
            'method' => 'onSerializerPostSerialize'
        ]];
    }
}