<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Board;
use Doctrine\ORM\EntityRepository;
use Threes\BoardInterface;
use Threes\BoardRepositoryInterface;

class BoardRepository extends EntityRepository implements BoardRepositoryInterface
{

    /**
     * @param BoardInterface $board
     * @return mixed
     */
    public function save(BoardInterface $board)
    {
        $boardEntity = $this->transformToEntity($board);
        $this->_em->persist($boardEntity);
    }

    public function findLastBoards($limit)
    {
        return $this->findBy([], ['createTime' => 'desc'], $limit);
    }

    /**
     * @return BoardInterface|null
     */
    public function findLastCreatedBoard()
    {
        return $this->findOneBy(['gameOver' => 0], ['updateTime' => 'desc', 'id' => 'desc']);
    }

    /**
     * @param BoardInterface $board
     * @return Board
     */
    private function transformToEntity(BoardInterface $board)
    {
        if ($board instanceof Board) {
            return $board;
        }
        $boardEntity = new Board($board);
        return $boardEntity;
    }

    public function onKernelTerminate()
    {
        $this->_em->flush();
    }
}
