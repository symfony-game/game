<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Tests\AppBundle\TruncateDatabaseOnSetupTrait;

class GameHistoryControllerTest extends WebTestCase
{
    use TruncateDatabaseOnSetupTrait;

    public function test_list()
    {
        $eventMock = $this->getMockBuilder(PostResponseEvent::class)->setMethods(['getResponse'])->disableOriginalConstructor()->getMock();
        $eventMock->expects($this->any())
            ->method('getResponse')
            ->willReturn(new Response());

        $client = static::createClient();
        $client->request('GET', '/game/new.json');
        static::$kernel->getContainer()->get('app.board_repository')->onKernelTerminate();
        $client->request('GET', '/game/new.json');
        static::$kernel->getContainer()->get('app.board_repository')->onKernelTerminate();
        $client->request('GET', '/game/new.json');
        static::$kernel->getContainer()->get('app.board_repository')->onKernelTerminate();
        $client->request('GET', '/game/boards.json');

        $list = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(3, $list);
    }
}
