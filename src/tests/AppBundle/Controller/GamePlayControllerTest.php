<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Tests\AppBundle\TruncateDatabaseOnSetupTrait;

class GamePlayControllerTest extends WebTestCase
{
    use TruncateDatabaseOnSetupTrait {
        setUp as truncateDatabase;
    }

    /**
     * @var Client
     */
    protected $client;

    protected function setUp()
    {
        $this->truncateDatabase();
        $this->client = static::createClient();
    }

    public function test_html_table()
    {
        $board = $this->newBoard();

        $this->assertArrayHasKey('grid', $board);
        $this->assertCount(4, $board['grid']);
        $this->assertCount(4, $board['grid'][0]);
    }


    public function test_new_grid_each_time()
    {
        $grid1 = $this->newBoard()['grid'];
        $grid2 = $this->newBoard()['grid'];
        $grid3 = $this->newBoard()['grid'];

        $this->assertFalse(($grid1 == $grid2) && ($grid2 == $grid3));
    }

    public function test_game_buttons()
    {
        $board = $this->newBoard();
        $this->assertEquals(['up', 'down', 'left', 'right'], array_keys($board['validMoves']));
    }

    public function test_game_link()
    {
        $board = $this->newBoard();
        $this->assertArrayHasKey('link', $board);
    }

    public function test_play_changes_the_grid()
    {
        $grid1 = $this->newBoard();
        $this->client->request('PATCH', '/game/up/play.json');
        $grid2 = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertNotEquals($grid1['grid'], $grid2['grid']);
    }

    private function newBoard()
    {
        $this->client->request('GET', '/game/new.json');
        return json_decode($this->client->getResponse()->getContent(), true);
    }
}
