<?php

namespace Tests\Threes;

use PHPUnit\Framework\TestCase;
use Threes\Board;
use Threes\BoardInterface;
use Threes\BoardRepositoryInterface;
use Threes\GamePlay;
use Threes\GameBoardFactory;

class GamePlayTest extends TestCase
{

    public function test_restart()
    {
        $board = $this->getMockBuilder(BoardInterface::class)->disableOriginalConstructor()->getMock();

        $factory = $this->getMockBuilder(GameBoardFactory::class)->getMock();
        $factory->expects($this->once())
            ->method('create')
            ->willReturn($board);

        $repository = $this->getMockBuilder(BoardRepositoryInterface::class)->getMock();
        $repository->expects($this->once())
            ->method('save')
            ->with($board);

        $game = new GamePlay($repository, $factory);

        $game->restart();
    }

    public function test_get_grid_of_last_board()
    {
        $board = (new GameBoardFactory())->create();
        $grid = $board->getGrid();

        $repository = $this->getMockBuilder(BoardRepositoryInterface::class)->getMock();
        $repository->expects($this->once())
            ->method('findLastCreatedBoard')
            ->willReturn($board);


        $game = new GamePlay($repository, $this->getMockBuilder(GameBoardFactory::class)->getMock());

        $this->assertEquals($grid, $game->getActiveBoard()->getGrid());
    }

    public function test_get_grid_without_last_board()
    {
        $board = new Board();

        $factory = $this->getMockBuilder(GameBoardFactory::class)->getMock();
        $factory->expects($this->any())
            ->method('create')
            ->willReturn($board);

        $repository = $this->getMockBuilder(BoardRepositoryInterface::class)->getMock();
        $repository->expects($this->once())
            ->method('findLastCreatedBoard')
            ->willReturn(null);


        $game = new GamePlay($repository, $factory);

        $this->assertEquals($board->getGrid(), $game->getActiveBoard()->getGrid());
    }

    public function test_perform_action()
    {
        $board = $this->getMockBuilder(Board::class)->getMock();

        $board->expects($this->any())
            ->method('isValidMove')
            ->willReturn(true);
        $board->expects($this->once())
            ->method('move')
            ->with('up');

        $repository = $this->getMockBuilder(BoardRepositoryInterface::class)->getMock();
        $repository->expects($this->once())
            ->method('findLastCreatedBoard')
            ->willReturn($board);

        $game = new GamePlay($repository, $this->getMockBuilder(GameBoardFactory::class)->getMock());

        $game->play('up');
    }
}
